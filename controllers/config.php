<?php
	/* Par convention, l'objet qui s'occupe d'une page doit avoir 
	 *
	 * le même nom, sinon ça ne marchera pas. ex : index.php et index
	 *
	 * Les propriéts sont : name  => String, title => String, viewsArray => Array, cssArray => Array, jsArray => Array
	 * 
	 * 
	 */

    $index = new Page(
    	
    	array(
    		'name' => 'index' ,
    		'title' => 'Home Page',
    		'viewsArray' => ["index_droit"],
    		'cssArray' => ["index_feu"]
    		));

?>