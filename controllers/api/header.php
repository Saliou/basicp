<?php 


    session_start();

    function classChargerCtrl( $classe )
    {
      require '../../models/'.$classe . '.php'; // On inclut la classe correspondante au paramètre passé.
    }
    
    require "../config/db_config.php"; // La base de donnée

    require "../config/functions.php"; 

    spl_autoload_register('classChargerCtrl'); 

    include "../../models/loadManager.php"; // Les managers de class

    

?>