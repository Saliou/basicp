<?php

/* 

	Class se chargeat de la gestion des pages
*/

class Page
{ 

	private $_name;
	private $_title;
	private $_jsArray;
	private $_cssArray;
	private $_viewsArray;

	public function objectType()
	{
		return "page";
	}


	//Hydrate

	public function hydrate(array $page)
	{
		if( isset($page["name"]) )
		{
			$this->setName( $page["name"] );
		}

		if( isset($page["title"]) )
		{
			$this->setTitle( $page["title"] );
		}

		if( isset($page["jsArray"]) )
		{
			$this->setJs( $page["jsArray"] );
		}
		else
		{	
			$this->setJs( [] );

		}

		if( isset($page["cssArray"]) )
		{
			$this->setCss( $page["cssArray"] );
		}
		else
		{
			$this->setCss( [] );
		}

		if( isset($page["viewsArray"]) )
		{
			$this->setViews( $page["viewsArray"] );
		}
		else
		{
			$this->setViews( [] );
		}


	}

	//Constructeur

	public function __construct($page = [])
	{
		$this->hydrate($page);		
	}
	//Liste des Getters

	public function name()
	{ 
		return $this->_name;
	}

	public function title()
	{ 
		return $this->_title;
	}

	public function jsFiles()
	{ 
		return $this->_jsArray;
	}

	public function cssFiles()
	{ 
		return $this->_cssArray;
	}

	


	//Liste des Setters

	public function setName($name)
	{ 
		$this->_name = $name ;
	}

	public function setTitle($title)
	{ 
		$this->_title = $title ;
	}

	public function setCss(Array $cssArray)
	{ 
		$this->_cssArray = $cssArray ;
	}

	public function setJs(Array $jsArray)
	{ 
		$this->_jsArray = $jsArray ;
	}

	public function setViews(Array $viewsArray)
	{ 
		$this->_viewsArray = $viewsArray ;
	}

	public function view( $view )
	{
		if( in_array($view, $this->_viewsArray) )
		{
			include "views/".$view.".php";
		}
		else
		{
			return false;
		}
	}
}

?>