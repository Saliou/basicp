<?php 

/*
+ + + + + + + + + + + + + + + + + + + + + + + + + + + + 
+
+	Fonction chargé de chargé une classe (cf autoload) 
+   
+   quand on y fait appel et qu'on la trouve pas inclut
+
+	@return void
+
+
+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + 
*/

function classCharger($classe)
{
  require 'models/'.$classe . '.php'; // On inclut la classe correspondante au paramètre passé.
}

/*
+ + + + + + + + + + + + + + + + + + + + + + + + + + + + 
+
+	Fonction chargé de générer els url du site
+
+	@return string
+
+
+ + + + + + + + + + + + + + + + + + + + + + + + + + + + + 
*/

function url($page = "home" ,$parametre = false, $path = "relative")
{
	$pathEnv = [ "dev" => "url_dev", "preprod" => "url_preprod", "prod" => "url_prod"];

	$url = '?page='.$page;

	if( $parametre )
	{
		foreach ($parametre as $key => $value)
		{
			$url.='&'.$key."=".$value;
		}
		//$url.='-'.$parametre["value"];	
	}
	if( $path != false )
	{
		if( $path == "absolute" )
		{
			return $pathEnv[APP_ENV]+""+$url;
		}

		else if( $path == "relative" )
		{
			return $url;
		}
	}
	else
	{
		return $url;
	}
}

function cryp( $string )
{ 
	return $string;
}

function decryp($string)
{
	return $string;
}