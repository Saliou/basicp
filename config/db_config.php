<?php


$db_conf = array(
	
	"dev" => array(

		"host" => "localhost",
		"name" => "nom_local", // Nom base de donnée
		"user" => "root", // Nom utilisateur base de donnée
		"pass" => "root", // Mot de passe base de donnée
		),
	
	"prod" => array(

		"host" => "host_distant",
		"name" => "name_distant",
		"user" => "user_distant",
		"pass" => "pass_distant",
		),

	"preprod" => array(

		"host" => "host_distant",
		"name" => "name_distant",
		"user" => "user_distant",
		"pass" => "pass_distant",
		)
	);

	try
	{
		$data_base = new PDO("mysql:host=".$db_conf[APP_ENV]["host"].";dbname=".$db_conf[APP_ENV]["name"],$db_conf[APP_ENV]["user"],$db_conf[APP_ENV]["pass"]);
	}
	
	catch(Exception $error)
	{
		die("Db_config Error : ".$error->getMessage());
	}
?>