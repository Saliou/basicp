<?php

	
	$nC=$name;

	$pre="";

	//$b=$a->fetch();

	$ma="";
	
	$ma.="<?php\n\nclass ".ucfirst($name)."Manager \n{ \n\n";

	$ma.="\tprivate \$_data_base;\n\n";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			if(preg_match($idRegex, $key))
			{
				$key=substr($key,4);

				$key=substr($key,0,-2);


				$ma.="\tprivate \$_".$key."Manager;\n\n";

			}

		}
	}

	$ma.="\tpublic function __construct(\$data_base)\n\t{";

	$ma.="\n\t\t\$this->setData_Base(\$data_base);\n";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			if(preg_match($idRegex, $key))
			{
				$key=substr($key,4);

				$key=substr($key,0,-2);

				$ma.="\n\t\t\$this->set".$key."Manager(new ".ucfirst($key)."Manager(\$data_base));\n";

			}

		}
	}

	$ma.="\t}\n";

	$ma.="\tpublic function add(".ucfirst($nC)." \$".$nC.")\n\t{\n\t\t";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			if(preg_match($idRegex, $key))
			{
				$prefixe=substr($key,0,3);

				$key=substr($key,4);

				$baseKey=substr($key,0,-2);

				$ma.="\n\t\t\$".$key."=\$this->_".$baseKey."Manager->add(\$".$name."->".$prefixe."_".$baseKey."());\n\n\t\t";

			}

		}
	}


	//partie pour le insert
	$ma.="\$query = \$this->_data_base->prepare('INSERT INTO ".$name."(";

	foreach ($b as $key => $value)
	{
		if(!is_int($key) AND !preg_match("#_id$#", $key) AND !preg_match("#_dateCreation$#", $key) AND !preg_match("#_deleted$#", $key))
		{
			$ma.=$key.",";	
		}
	}

	$ma=substr($ma,0,-1); //enlever la dernièer virgule;

	$ma.=") VALUES ( ";

	foreach ($b as $key => $value)
	{
		if(!is_int($key) AND !preg_match("#_id$#", $key) AND !preg_match("#_dateCreation$#", $key) AND !preg_match("#_deleted$#", $key))
		{
			$ma.=":".substr($key,4).",";	
		}
	}


	$ma=substr($ma,0,-1); //enlever la dernièer virgule;

	$ma.=")');";
	
	$ma.="\n\n\t\t\$query->execute(array( ";

	foreach ($b as $key => $value)
	{
		if(!is_int($key) AND !preg_match("#_id$#", $key) AND !preg_match("#_dateCreation$#", $key) AND !preg_match("#_deleted$#", $key))
		{
			if(preg_match($idRegex, $key))
			{
				$prefixe=substr($key,0,3);

				$key=substr($key,4);

				$baseKey=substr($key,0,-2);

				$ma.="'".$key."' => \$".$key." ,";	


			}
			else
			{
				$pre=$key;
				
				$ma.="'".substr($key,4)."' => \$".$name."->".$key."() ,";	
			}
				
		}
	}

	$ma=substr($ma,0,-1); //enlever la dernièer virgule;


	$ma.="));";
	
	$ma.="\n\n\t\tif(\$query->rowCount()==1 AND \$query->errorCode()==0)";

	$ma.="\n\t\t{\n";

	$ma.="\t\t\treturn \$this->_data_base->lastInsertId();";

	$ma.="\n\t\t}\n";

	$ma.="\t\telse";
	
	$ma.="\n\t\t{\n";
	
	$ma.="\t\t\treturn \$query->errorCode();";

	$ma.="\n\t\t}\n\n";
	$ma.="\n\t}\n\n";


	$ma.="\tpublic function delete(".ucfirst($nC)." \$".$nC.")\n\t{\n\t\t";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			if(preg_match($idRegex, $key))
			{
				$prefixe=substr($key,0,3);

				$key=substr($key,4);

				$baseKey=substr($key,0,-2);

				$ma.="\n\t\t\$".substr($key,0,-2)."Deleted=\$this->_".$baseKey."Manager->delete(\$".$name."->".$prefixe."_".$baseKey."());\n\n\t\t";

			}

		}
	}


	//partie pour le delete
	$ma.="\$query = \$this->_data_base->prepare('UPDATE ".$name." SET ".substr($pre,0,4)."deleted=1 WHERE ".substr($pre,0,4)."id = '.\$".$name."->".substr($pre,0,4)."id());";

		
	$ma.="\n\n\t\t\$query->execute();";
	
	$ma.="\n\n\t\tif(\$query->rowCount()==1 AND \$query->errorCode()==0)";

	$ma.="\n\t\t{\n";

	$ma.="\t\t\treturn true;";

	$ma.="\n\t\t}\n";

	$ma.="\t\telse";
	
	$ma.="\n\t\t{\n";
	
	$ma.="\t\t\treturn \$query->errorCode();";

	$ma.="\n\t\t}\n\n";
	
	$ma.="\n\t}\n\n";

	$ma.="\tpublic function update(".ucfirst($nC)." \$".$nC.")\n\t{\n\t\t";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			if(preg_match($idRegex, $key))
			{
				$prefixe=substr($key,0,3);

				$key=substr($key,4);

				$baseKey=substr($key,0,-2);

				$ma.="\n\t\t\$".substr($key,0,-2)."Updated=\$this->_".$baseKey."Manager->update(\$".$name."->".$prefixe."_".$baseKey."());\n\n\t\t";

			}

		}
	}

	//partie pour le update

	$ma.="\$query = \$this->_data_base->prepare('UPDATE ".$name." SET ";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			$pre=substr($key,0,3);
			
			$ma.=$key."=:".substr($key,4).",";	
		}
	}

	$ma=substr($ma,0,-1); //enlever la dernièer virgule;

	$ma.=" WHERE ".$pre."_id='.\$user->".$pre."_id() ";

	$ma.=");";
	
	$ma.="\n\n\t\t\$query->execute(array( ";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			$pre=$key;

			if(!preg_match($idRegex, $key))
			{
				$ma.="'".substr($key,4)."' => \$".$name."->".$key."() ,";	

			}
			else
			{
				$pre=$key;
				
				$ma.="'".substr($key,4)."' => \$".$name."->".substr($key,0,-2)."() ,";	
			}
				
		}
	}

	$ma=substr($ma,0,-1); //enlever la dernièer virgule;


	$ma.="));";
		
	$ma.="\n\n\t\t\$query->execute();";
	
	$ma.="\n\n\t\tif(\$query->rowCount()==1 AND \$query->errorCode()==0)";

	$ma.="\n\t\t{\n";

	$ma.="\t\t\treturn true;";

	$ma.="\n\t\t}\n";

	$ma.="\t\telse";
	
	$ma.="\n\t\t{\n";
	
	$ma.="\t\t\treturn \$query->errorCode();";

	$ma.="\n\t\t}\n\n";

	$ma.="\n\t}\n\n";

	//partie pour le get

	$ma.="\tpublic function get(\$id)\n\t{\n\t\t";

	$ma.="\$query = \$this->_data_base->prepare('SELECT * FROM ".$name." WHERE ".substr($pre,0,4)."deleted=0 AND ".substr($pre,0,4)."id='.\$id);";
	
	$ma.="\n\n\t\t\$query->execute();";
	
	$ma.="\n\n\t\tif(\$query->rowCount()==1 AND \$query->errorCode()==0)";

	$ma.="\n\t\t{\n";
	
	$ma.="\t\t\t\$re=\$query->fetch();\n\n";

	foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{	
			$pre=$key;

			if(preg_match($idRegex, $key))
			{
				$ma.="\t\t\t\$re[\"".substr($key,0,-2)."\"]=\$this->_".substr(substr($key,4),0,-2)."Manager->get(\$re[\"".$key."\"]);\n\n";

			}		
		}
	}
	$ma.="\t\t\treturn \$re;\n\n";

	$ma.="\t\t}\n";

	$ma.="\t\telse";

	$ma.="\n\t\t{\n";
	
	$ma.="\t\t\treturn false;";

	$ma.="\n\t\t}";

	$ma.="\n\t}\n\n";


	//partie set db

	$ma.="\tpublic function setData_Base(PDO \$db)\n\t{\n\t\t";	
	
	$ma.="\$this->_data_base = \$db;";

	$ma.="\n\t}\n\n";

	 foreach ($b as $key => $value)
	{
		if(!is_int($key))
		{
			$pre=$key;

			if(preg_match($idRegex, $key))
			{
				$bn=substr($key,4);

				$ma.="\tpublic function set".substr($bn,0,-2)."Manager(".ucfirst(substr($bn,0,-2))."Manager \$".substr($bn,0,-2).")\n\t{\n\t\t";	
				
				$ma.="\$this->_".substr($bn,0,-2)."Manager = \$".substr($bn,0,-2).";";

				$ma.="\n\t}\n\n";
			}
		
		}
	}

	$ma.="\n}\n?>";





?>