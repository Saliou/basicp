<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->

<!--<![endif]-->

<html>
	<head>
	
		<!-- Google Analytics Start -->

		<!-- Google Analytics End -->


		<title><?php echo $$page->title() ?></title>
		
		<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=8">
		<![endif]-->
	
		<!-- Metas -->
		<meta charset="utf-8">
		  
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

		<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- ACTIVE LE RESPONSIVE AVEC LES DIFFERENTS CSS -->

		<meta name="author" content="<?php echo APP_AUTHOR; ?>">

		<meta name="robots" content="all" />

		<link rel="profile" href="http://gmpg.org/xfn/11">

			
		<!--Font Awesome -->
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">

		<!-- JQUERY UI -->
		<link rel="stylesheet" type="text/css" href="assets/jquery-ui-1.11.4/jquery-ui.min.css">
		
		<!-- CSS / BootStrap -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet"> <!-- Bootstrap -->

		<link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet"> <!-- Bootstrap -->
			
		<!-- Favicon -->
		<link rel="shortcut icon"  type="image/png" href="assets/images/favicon.png" />


		<!-- CSS -->

		<link rel="stylesheet" type="text/css" href="assets/css/layout.css">	
		
		<link rel="stylesheet" type="text/css" href="assets/css/header.css">

		<?php

			for ($i=0; $i < count($$page->cssFiles()) ; $i++)
			{ 
				if( file_exists("assets/css/".$$page->cssFiles()[$i].".css") )
				{
					?>
						<link rel="stylesheet" type="text/css" href="assets/css/<?php echo $$page->cssFiles()[$i]; ?>.css">	

					<?php
				}
			}

			if( file_exists("assets/css/".$page.".css") )
			{
				?>
					<link rel="stylesheet" type="text/css" href="assets/css/<?php echo $page; ?>.css">	

				<?php
			}
		?>
		
		

		<!-- JS -->
		
		<script src="assets/js/jquery.js"></script>

		<?php include "assets/js/class.php" ?>
		

		<script src="assets/js/layout.js"></script> 
	
		<script src="assets/js/header.js"></script> 
		
</head>

<body>

