<?php

include "config/functions.php";

spl_autoload_register('classCharger'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée.

session_start();

ini_set('session.bug_compat_warn', 0);

ini_set('session.bug_compat_42', 0);

define ("_dirviews","views/pages/");	

define ("_dirctrl","views/controllers/");

require 'config/app_conf.php';

switch ( APP_ENV )
{
	case 'dev':
		
		ini_set('error_reporting', E_ALL);

		break;

	case 'preprod':
		
		ini_set('error_reporting', E_ALL);
		
		break;

	case 'prod':
		
		ini_set('error_reporting', 0);
			
		break;
	
	default:
		# code...
		break;
}

include 'config/class-config/loadClassConfig.php';

include 'config/db_config.php';


if (!isset($_GET['page']))
{
	$page = APP_DEFAULT_PAGE;

	$_SESSION["page"] = $page;
} 

else 
{
	$page = $_GET['page'];

	$_SESSION["page"] = $page;
	
}

?>