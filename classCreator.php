<?php


	function createClassFileFromDb($name)
	{
		require "config/db_config.php";

		$nC=$name;

		$nomPage ;


		$a=$data_base->prepare("SELECT * FROM ".$nC);
		
		$a->execute();

		if($a->rowCount()>0)
		{
			$b=$a->fetch();
 
			$s="";
			
			$s.="<?php\n\n/* Ne surtout pas croire que c'est une classe parfaite, elle a juste\nles bases il ne faut surtout pas oublier de l'adapter en fonction\nde ses besoins.\n\tLe Templater de Class ;)\n*/\n\nclass ".ucfirst($name)."\n{ \n\n";

			foreach ($b as $key => $value)
			{
				if(!is_int($key))
				{
					if(preg_match($idRegex, $key))
					{
						$s.="\tprivate \$_".substr($key, 0, -2).";\n";	
					}					
					else
					{
						$s.="\tprivate \$_".$key.";\n";	

					}
				}
			}

			$s.="\n\n\t//Hydrate\n\n";

			$s.="\tpublic function hydrate(array \$".$name.")\n\t{";

			foreach ($b as $key => $value)
			{
				if(!is_int($key))
				{
					if(preg_match($idRegex, $key))
					{
						$key=substr($key,0,-2);
					}

					$s.="\n\t\tif(isset(\$".$name."['".$key."']))\n\t\t{\n\t\t\t \$this->set".$key."(\$".$name."['".$key."']);\n\t\t}\n";

					
				}
			}

			$s.="\t}";

			$s.="\n\n\t//Constructeur\n\n";

			$s.="\tpublic function __construct(array \$".$name.")\n\t{";

			$s.="\n\t\t\$this->hydrate(\$".$name.");\t\t\n";
			

			$s.="\t}";


			$s.="\n\t//Liste des Getters\n\n";


			foreach ($b as $key => $value)
			{
				if(!is_int($key))
				{
					if(preg_match($idRegex, $key))
					{
						$key=substr($key,0,-2);
					}

					$s.="\tpublic function ".$key."()\n\t{ \n\t\treturn \$this->_".$key.";\n\t}\n\n";
				}
			}


			$s.="\n\t//Liste des Setters\n\n";


			foreach ($b as $key => $value)
			{
				if(!is_int($key))
				{
					if(preg_match($idRegex, $key))
					{
						$key=substr($key,0,-2);
						
						$s.="\tpublic function set".$key."(array \$".$key.")\n\t{ \n\t\t\$this->_".$key."->hydrate(\$".$key.");\n\t}\n\n";

					}
					else
					{
						$s.="\tpublic function set".$key."(\$".$key.")\n\t{ \n\t\t\$this->_".$key." = \$".$key." ;\n\t}\n\n";

					}

				}
			}
					include "managerCreator.php";
					
					include "classJsCreator.php";

			$s.="}\n\n?>";

			$sd=fopen("config/class/".ucfirst($nC).".php", "x+");
			
			$smanager=fopen("config/class/".ucfirst($nC)."Manager.php", "x+");
			
			fputs($smanager,$ma);
			
			fclose($smanager);

			
			$classJs=fopen("js/class.js", "a+");
			
			fputs($classJs,$jsClass);
			
			fclose($classJs);

			$controller = fopen("controller/content/".$nomPage);
			
			$views = fopen("views/".$nomPage);
			
			$css = fopen("assets/css/".$nomPage);
			
			$js = fopen("assets/js/".$nomPage);

			for ($i=0; $i < $filesTocreate ; $i++)
			{ 
				if( file_exists($filesTocreate[$i]["url"]) )
				{
					$file =  fopen($filesTocreate[$i]["url"], "x+");
					
					fputs($file,$filesTocreate[$i]["content"]);
					
					fclose($file);
				}
				else
				{
					// Fichier déja existant
				}
			}
			
			$pageConfig = fopen("controllers/config.php","a+");
			
			if($sd)
			{
				fputs($sd,$s);
				
				fclose($sd);
			}
			else
			{
				return -1;
			}
			
		}
		else
		{
			return -2;
		}
		
			
	}


		
	//synthax d'une classe
?>
