<?php 
	function jsFileMaker( $name )
	{
		$jsFileString = "//VARIABLE\n\n\tvar page = '".$name."'; \n\n"; 
		
		$jsFileString .= "//FUNCTION\n\n";

		$jsFileString .= "//EVENT\n\n";

		$jsFileString .= "//READY\n\n"; 

		$jsFileString .= "\t\$(document).ready(function()\n\t{\n\t\n\n\t})";

		return $jsFileString;
	}

	function createPage( $name )
	{
		/* Création des fichiers */

		$cssFile = fopen("assets/css/".$name.".css", "x+");

		$cssMobileFile = fopen("assets/css/".$name."_mobile.css", "x+");

		$jsFile = fopen("assets/js/".$name.".js", "x+");

		$phpFile = fopen("controllers/content/pages/".$name.".php", "x+");

		$viewFile = fopen("views/pages/".$name.".php", "x+");

		//mkdir("controllers/ajax/".$name);
			
		$controller = fopen("controllers/api/".$name."/exampleCtrl.php", "x+");

		$phpFileString = "<?php\n\tinclude _dirviews.'".$name.".php'; \n?>";
		
		fputs($phpFile,$phpFileString);
		
		fputs($jsFile,jsFileMaker($name));

		/* On ferme tous les pointeurs de fichier*/

		fclose($cssFile);

		fclose($cssMobileFile);

		fclose($jsFile);

		fclose($phpFile);
		
		fclose($viewFile);
	}
	
	//checker dabord si la page n'existe pas
	//checker si le nom n'existe pas
	
	//créer le fichier js css et la vue avec le nom de la page par défaut 
	// créer l'objet Page correspondant à cette page
	//créer le dossier correspondant à ce dossier dns les controllers

	if( isset($_GET["page"]) )
	{
		if( !file_exists("controllers/content/".$_GET["page"].".php") )
		{
			createPage( $_GET["page"] );
		}
		else
		{	
			echo "File already here man !";
		}
	}
?>