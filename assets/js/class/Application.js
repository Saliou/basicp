function Application(env) //Parametre
{
	
	//Attributs

	this.env = env == undefined || env == null ? "dev" : env;

	this.root = {"dev" : "#Lien de la dev #", "preprod" : "#Lien de la dev #", "preprod" : "#Lien de la prod #" };

	//Méthodes
	
	
	this.capitalize = function( $string )
	{
	    return $string.charAt(0).toUpperCase() + $string.slice(1);
	}

	this.isMail = function( $string )
	{
		var regexMail =  /([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i //Regex de mail
		
		return regexMail.test($string.slice(0));
	}

	this.isName = function( $string )
	{
		var regexNom = /^[a-z]+[ \-']?[[a-z]+[ \-']?]*[a-z]+$/gi; //Regex si c'est un nom propre
		
	    return regexNom.test($string.slice(0));
	}

	this.isInt =  function (n)
	{

    	return Number(n) === n && n % 1 === 0;
	}

	this.isFloat = function (n)
	{
	    return n === Number(n) && n % 1 !== 0;
	}

	this.isSet = function( variable)
	{
		return (variable != null && variable != undefined && typeof(variable) != undefined);
	}
	
	this.url = function (page,parametre,path)
	{
		path = path == undefined || path == null ? false : path;

		var pathEnv = { "dev" : "/listenetwork.com/listen/" , "prod" : "/", "preprod" : "/listenetwork.com/listen/"  };

		var url = '?page='+page;

		if(parametre != false && parametre != undefined)
		{
			url += '&'+parametre["nom"]+"="+parametre["value"];
			//url.='-'.$parametre["value"];
		}

		if( path != false )
		{
			if( path == "absolute" )
			{
				return pathEnv[envi]+""+url;
			}

			else if( path == "relative" )
			{
				return url;
			}
		}
		else
		{
			return url;
		}
	}
	
}


